export class Line {

	private deltaX: number;
	private deltaY: number;

	public constructor(
		private nearX: number,
		private nearY: number,
		private farX: number,
		private farY: number
	) {
		this.deltaX = farX - nearX;
		this.deltaY = farY - nearY;
	}

	public clone(): Line {
		return new Line(this.nearX, this.nearY, this.farX, this.farY);
	}

	public setNearPoint(nearX: number, nearY: number): void {
		this.nearX = nearX;
		this.nearY = nearY;

		this.deltaX = this.farX - this.nearX;
		this.deltaY = this.farY - this.nearY;
	}

	public setFarPoint(farX: number, farY: number): void {
		this.farX = farX;
		this.farY = farY;

		this.deltaX = this.farX - this.nearX;
		this.deltaY = this.farY - this.nearY;
	}

	public isBelow(x: number, y: number): boolean {
		return this.calculateRelativeSlope(x, y) > 0;
	}

	public isBelowOrCollinear(x: number, y: number): boolean {
		return this.calculateRelativeSlope(x, y) >= 0;
	}

	public isAbove(x: number, y: number): boolean {
		return this.calculateRelativeSlope(x, y) < 0;
	}

	public isAboveOrCollinear(x: number, y: number): boolean {
		return this.calculateRelativeSlope(x, y) <= 0;
	}

	public isCollinear(x: number, y: number): boolean {
		return this.calculateRelativeSlope(x, y) === 0;
	}

	public isLineCollinear(line: Line): boolean {
		return this.isCollinear(line.nearX, line.nearY) && this.isCollinear(line.farX, line.farY);
	}

	private calculateRelativeSlope(x: number, y: number): number {
		return this.deltaY * (this.farX - x) - this.deltaX * (this.farY - y);
	}
}
