export class ViewBump {

	public constructor(
		public readonly x: number,
		public readonly y: number,
		public readonly parent?: ViewBump
	) {}

	public clone(): ViewBump {
		return new ViewBump(this.x, this.y, this.parent ? this.parent.clone() : undefined);
	}
}
