import { IsTransparentFunc } from "./IsTransparentFunc";
import { SetVisibleFunc } from "./SetVisibleFunc";
import { View } from "./View";
import { Line } from "./Line";
import { Point } from "./Point";
import { ViewBump } from "./ViewBump";

interface FovData {
	setVisible: SetVisibleFunc;
	isTransparent: IsTransparentFunc;
	startX: number;
	startY: number;
	radius: number;
	visited: Array<boolean>;
}

type Delta = -1 | 1;

export class PermissiveFov {

	public constructor(
		private mapWidth: number,
		private mapHeight: number,
		private isTransparent: IsTransparentFunc
	) {}

	public compute(startX: number, startY: number, radius: number, setVisible: SetVisibleFunc) {
		var data: FovData = {
			setVisible,
			isTransparent: this.isTransparent,
			startX,
			startY,
			radius,
			visited: []
		};

		// Will always see the centre.
		data.setVisible(startX, startY);
		data.visited[this.mapWidth * startY + startX] = true;

		// Get the dimensions of the actual field of view, making sure not to go off the map or beyond the radius.
		const minExtentX: number = (startX < radius ? startX : radius);
		const maxExtentX: number = (this.mapWidth - startX <= radius ? this.mapWidth - startX - 1 : radius);
		const minExtentY: number = (startY < radius ? startY : radius);
		const maxExtentY: number = (this.mapHeight - startY <= radius ? this.mapHeight - startY - 1 : radius);

		this.computeQuadrant(data, 1, 1, maxExtentX, maxExtentY);
		this.computeQuadrant(data, 1, -1, maxExtentX, minExtentY);
		this.computeQuadrant(data, -1, -1, minExtentX, minExtentY);
		this.computeQuadrant(data, -1, 1, minExtentX, maxExtentY);
	};

	public setMapDimensions(mapWidth: number, mapHeight: number): void {
		this.mapWidth = mapWidth;
		this.mapHeight = mapHeight;
	};

	public setIsTransparent(isTransparent: IsTransparentFunc): void {
		this.isTransparent = isTransparent;
	};

	private computeQuadrant(data: FovData, deltaX: Delta, deltaY: Delta, maxX: number, maxY: number) {
		const activeViews: Array<View> = [];
		let startJ: number;
		let maxJ: number;
		let maxI: number = maxX + maxY;
		const shallowLine: Line = new Line(0, 1, maxX, 0);
		const steepLine: Line = new Line(1, 0, 0, maxY);
		let viewIndex: number = 0;

		activeViews.push(new View(shallowLine, steepLine));

		// Visit the tiles diagonally and going outwards
		//
		// .
		// .
		// 9
		// 58
		// 247
		// @136..
		for (let i = 1; i <= maxI && activeViews.length; ++i) {
			startJ = (0 > i - maxX ? 0 : i - maxX);
			maxJ = (i < maxY ? i : maxY);

			for (let j = startJ; j <= maxJ && viewIndex < activeViews.length; ++j) {
				this.visitPoint(data, i - j, j, deltaX, deltaY, viewIndex, activeViews);
			}
		}
	}

	private visitPoint(
		data: FovData,
		x: number, y: number,
		deltaX: Delta, deltaY: Delta,
		viewIndex: number,
		activeViews: Array<View>
	) {
		// The top left and bottom right corners of the current coordinate.
		const topLeft: Point = new Point(x, y + 1);
		const bottomRight: Point = new Point(x + 1, y);

		// The real quadrant coordinates
		const realX: number = x * deltaX;
		const realY: number = y * deltaY;

		for (; viewIndex < activeViews.length &&
			activeViews[viewIndex].steepLine.isBelowOrCollinear(bottomRight.x, bottomRight.y); ++viewIndex) {
			// The current coordinate is above the current view and is ignored. The steeper fields may need it though.
		}

		if (
			viewIndex === activeViews.length ||
			activeViews[viewIndex].shallowLine.isAboveOrCollinear(topLeft.x, topLeft.y)
		) {
			// Either the current coordinate is above all of the fields or it is below all of the fields.
			return;
		}

		// It is now known that the current coordinate is between the steep
		// and shallow lines of the current view.

		const visitedPoint: Point = new Point(data.startX + realX, data.startY + realY);
		const visitedPointIndex = this.mapWidth * visitedPoint.y + visitedPoint.x;
		if (!data.visited[visitedPointIndex]) {
			data.visited[visitedPointIndex] = true;
			data.setVisible(visitedPoint.x, visitedPoint.y);
		}

		if (data.isTransparent(visitedPoint.x, visitedPoint.y)) {
			// The current coordinate does not block sight and therefore has no effect on the view.
			return;
		}

		// The current coordinate is an obstacle.
		const shallowLineIsAboveBottomRight = activeViews[viewIndex].shallowLine.isAbove(bottomRight.x, bottomRight.y);
		const steepLineIsBelowTopLeft = activeViews[viewIndex].steepLine.isBelow(topLeft.x, topLeft.y);

		if (shallowLineIsAboveBottomRight && steepLineIsBelowTopLeft) {
			// The obstacle is intersected by both lines in the current view. The view is completely blocked.
			activeViews.splice(viewIndex, 1);
		} else if (shallowLineIsAboveBottomRight) {
			// The obstacle is intersected by the shallow line of the current view. The shallow line needs to be raised.
			this.addShallowBump(topLeft.x, topLeft.y, activeViews, viewIndex);
			this.checkView(activeViews, viewIndex);
		} else if (steepLineIsBelowTopLeft) {
			// The obstacle is intersected by the steep line of the current view. The steep line needs to be lowered.
			this.addSteepBump(bottomRight.x, bottomRight.y, activeViews, viewIndex);
			this.checkView(activeViews, viewIndex);
		} else {
			// The obstacle is completely between the two lines of the current view.
			// Split the current view into two views above and below the current coordinate.
			const shallowViewIndex = viewIndex;
			let steepViewIndex = ++viewIndex;

			activeViews.splice(shallowViewIndex, 0, activeViews[shallowViewIndex].clone());
			this.addSteepBump(bottomRight.x, bottomRight.y, activeViews, shallowViewIndex);

			if (!this.checkView(activeViews, shallowViewIndex)) {
				--steepViewIndex;
			}

			this.addShallowBump(topLeft.x, topLeft.y, activeViews, steepViewIndex);
			this.checkView(activeViews, steepViewIndex);
		}
	}

	private addShallowBump(x: number, y: number, activeViews: Array<View>, viewIndex: number): void {
		activeViews[viewIndex].shallowLine.setFarPoint(x, y);
		activeViews[viewIndex].shallowBump = new ViewBump(x, y, activeViews[viewIndex].shallowBump);

		for (var curBump = activeViews[viewIndex].steepBump; curBump; curBump = curBump.parent) {
			if (activeViews[viewIndex].shallowLine.isAbove(curBump.x, curBump.y)) {
				activeViews[viewIndex].shallowLine.setNearPoint(curBump.x, curBump.y);
			}
		}
	}

	private addSteepBump(x: number, y: number, activeViews: Array<View>, viewIndex: number): void {
		activeViews[viewIndex].steepLine.setFarPoint(x, y);
		activeViews[viewIndex].steepBump = new ViewBump(x, y, activeViews[viewIndex].steepBump);

		for (var curBump = activeViews[viewIndex].shallowBump; curBump; curBump = curBump.parent) {
			if (activeViews[viewIndex].steepLine.isBelow(curBump.x, curBump.y)) {
				activeViews[viewIndex].steepLine.setNearPoint(curBump.x, curBump.y);
			}
		}
	}

	private checkView(activeViews: Array<View>, viewIndex: number): boolean {
		/* Remove the view in activeViews at index viewIndex if either:
		 *     - the two lines are collinear
		 *     - the lines pass through either extremity
		 */
		if (
			activeViews[viewIndex].shallowLine.isLineCollinear(activeViews[viewIndex].steepLine) &&
			(activeViews[viewIndex].shallowLine.isCollinear(0, 1)
				|| activeViews[viewIndex].shallowLine.isCollinear(1, 0))
		) {
			activeViews.splice(viewIndex, 1);
			return false;
		}

		return true;
	}
}
