import { Line } from "./Line";
import { ViewBump } from "./ViewBump";

export class View {

	public constructor(
		public readonly shallowLine: Line,
		public readonly steepLine: Line,
		public shallowBump?: ViewBump,
		public steepBump?: ViewBump
	) {}

	public clone(): View {
		return new View(
			this.shallowLine.clone(),
			this.steepLine.clone(),
			this.shallowBump ? this.shallowBump.clone() : undefined,
			this.steepBump ? this.steepBump.clone() : undefined
		);
	}
}
