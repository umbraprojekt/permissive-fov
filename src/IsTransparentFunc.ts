export type IsTransparentFunc = (x: number, y: number) => boolean;
