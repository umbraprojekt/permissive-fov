export declare class ViewBump {
    readonly x: number;
    readonly y: number;
    readonly parent?: ViewBump;
    constructor(x: number, y: number, parent?: ViewBump);
    clone(): ViewBump;
}
