export declare class Line {
    private nearX;
    private nearY;
    private farX;
    private farY;
    private deltaX;
    private deltaY;
    constructor(nearX: number, nearY: number, farX: number, farY: number);
    clone(): Line;
    setNearPoint(nearX: number, nearY: number): void;
    setFarPoint(farX: number, farY: number): void;
    isBelow(x: number, y: number): boolean;
    isBelowOrCollinear(x: number, y: number): boolean;
    isAbove(x: number, y: number): boolean;
    isAboveOrCollinear(x: number, y: number): boolean;
    isCollinear(x: number, y: number): boolean;
    isLineCollinear(line: Line): boolean;
    private calculateRelativeSlope;
}
