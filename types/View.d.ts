import { Line } from "./Line";
import { ViewBump } from "./ViewBump";
export declare class View {
    readonly shallowLine: Line;
    readonly steepLine: Line;
    shallowBump?: ViewBump;
    steepBump?: ViewBump;
    constructor(shallowLine: Line, steepLine: Line, shallowBump?: ViewBump, steepBump?: ViewBump);
    clone(): View;
}
