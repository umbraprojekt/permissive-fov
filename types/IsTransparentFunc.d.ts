export declare type IsTransparentFunc = (x: number, y: number) => boolean;
