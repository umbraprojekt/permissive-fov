export declare class Point {
    readonly x: number;
    readonly y: number;
    constructor(x: number, y: number);
}
