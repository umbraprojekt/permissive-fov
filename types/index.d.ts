import { IsTransparentFunc } from "./IsTransparentFunc";
import { SetVisibleFunc } from "./SetVisibleFunc";
export declare class PermissiveFov {
    private mapWidth;
    private mapHeight;
    private isTransparent;
    constructor(mapWidth: number, mapHeight: number, isTransparent: IsTransparentFunc);
    compute(startX: number, startY: number, radius: number, setVisible: SetVisibleFunc): void;
    setMapDimensions(mapWidth: number, mapHeight: number): void;
    setIsTransparent(isTransparent: IsTransparentFunc): void;
    private computeQuadrant;
    private visitPoint;
    private addShallowBump;
    private addSteepBump;
    private checkView;
}
