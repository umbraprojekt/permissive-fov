# Changelog

## 2.0.0
### Improvements
* Visited cell count mechanism adjusted for an up to 20% performance boost.
### New features
* Ported to TypeScript
### API break
* Exported module is now named. Call `require("permissive-fov").PermissiveFov` to import.

## 1.0.1
* Readme and package.json improvements

## 1.0.0
* Initial release
